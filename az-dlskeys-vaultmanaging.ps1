# login to Azure account 
Connect-AzAccount

# select the subscription to work in, deploying resources
Get-AzSubscription -SubscriptionName "Azure for Students" | Select-AzSubscription

$rg = Get-AzResourceGroup -Name "sentia-we-az-dev-rg"

$dls = Get-AzStorageAccount -ResourceGroupName $rg.ResourceGroupName -Name "sentiawedevdls"

# KeyVault application Id across all azure platform
$keyVaultAppId = "cfa8b339-82a2-471a-a3c9-0fc0be7a4093"

$keyvault = Get-AzKeyVault -ResourceGroupName $rg.ResourceGroupName -VaultName "sentia-we-dev-keyvault"

$storageAccountKey = "key1"
# get your userprincipal id - logged user
$userId = (Get-AzContext).Account.Id
$recyclePeriod = [System.Timespan]::FromDays(90)

# Assigning RBAC role "Storage Account Key Operator Service Role" to Key Vault
New-AzRoleAssignment -ApplicationId $keyVaultAppId -RoleDefinitionName "Storage Account Key Operator Service Role" -Scope $dls.Id

# Giving our user access to all storage account permissions, on the Key Vault instance
Set-AzKeyVaultAccessPolicy -VaultName $keyvault.VaultName -UserPrincipalName $userId -PermissionsToStorage get, list, delete, set, update, regeneratekey, getsas, listsas, deletesas, setsas, recover, backup, restore, purge

# Adding our storage account to your Key Vault's managed storage accounts. Specifying regenerating period key of every 90 days.

Add-AzKeyVaultManagedStorageAccount -VaultName $keyvault.VaultName -AccountName $dls.StorageAccountName -AccountResourceId $dls.Id -ActiveKeyName $storageAccountKey -RegenerationPeriod $recyclePeriod

############# SAS Token for accessing SA ##############

$storageContext = New-AzStorageContext -StorageAccountName "sentiawedevdls" -Protocol Https -StorageAccountKey key1
$startSAS = [System.DateTime]::Now.Date
$endSAS = [System.DateTime]::Now.AddMonths(1)

$sasToken = New-AzStorageAccountSasToken -Service blob -ResourceType Service,Container,Object -Permission "racwdlup" -Protocol HttpsOnly -StartTime $startSAS -ExpiryTime $endSAS -Context $storageContext