...

This is my Azure Assessment demo made for Sentia project. 

* 10th February, 2020 *

All my ARM templates build and used I put on this public repo, including also some azure powershell scripts to perform several tasks.

I have done to many changes done to this json templates and this project, improving as much as I know (ofc can be much better, complex and organized).

I wrote down a simple doc ("Tutorial-AzAssessmentDemoSentia.docx"), like a guide tutorial explaining briefly the content of templates and other information related.

For anything else, it's all in my mind and I will gladly share and discuss all the process I have gone throught and decision making.

Thanks and looking forward to hearing from you soon.

Kind regards,

Ermir HOXHAJ

