# login to your azure account
Connect-AzAccount

# login to your azure active directory tenant
Connect-AzureAD

# create the new custom role, specified in json file
New-AzRoleDefinition -InputFile .\az-rbac-custom.json

$role = Get-AzRoleDefinition | Where-Object {$_.Name -like 'Sentia*'}

$group = Get-AzADGroup | Where-Object {$_.DisplayName -like 'Sentia Azure*'}

# assign the custom role to a group object in aad
New-AzRoleAssignment -ObjectId $group.Id -RoleDefinitionId $role.Id -Scope "/subscriptions/44aebd3f-1566-45ad-88b7-782b50bba269/"

Get-AzRoleAssignment -ObjectId $group.Id