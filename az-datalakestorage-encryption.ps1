# login to Azure account 
Connect-AzAccount

# select the subscription to work in, deploying resources
Get-AzSubscription -SubscriptionName "Azure for Students" | Select-AzSubscription

$rg = Get-AzResourceGroup -Name "sentia-we-az-dev-rg"

$dls = Get-AzStorageAccount -ResourceGroupName $rg.ResourceGroupName -Name "sentiawedevdls"

$dlsPrincipal = Set-AzStorageAccount -ResourceGroupName $rg.ResourceGroupName -Name $dls.StorageAccountName -AssignIdentity

$dlsPrincipal.Identity.PrincipalId

$keyvault = Get-AzKeyVault -ResourceGroupName $rg.ResourceGroupName -VaultName "sentia-we-dev-keyvault"
$key = Get-AzKeyVaultKey -VaultName $keyvault.VaultName -Name "dlskey"

Set-AzKeyVaultAccessPolicy -VaultName $keyvault.VaultName -ObjectId $dlsPrincipal.Identity.PrincipalId -PermissionsToKeys wrapkey,unwrapkey,get,recover

Set-AzStorageAccount -Name $dls.StorageAccountName -ResourceGroupName $rg.ResourceGroupName -KeyvaultEncryption -KeyVaultUri $keyvault.VaultUri -KeyName $key.Name -KeyVersion $key.Version

$dls.Encryption