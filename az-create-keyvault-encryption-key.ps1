# login to Azure account 
Connect-AzAccount

# select the subscription to work in, deploying resources
Get-AzSubscription -SubscriptionName "Azure for Students" | Select-AzSubscription

$rg = Get-AzResourceGroup -Name "sentia-we-az-dev-rg"
$keyvault = Get-AzKeyVault -ResourceGroupName $rg.ResourceGroupName -VaultName "sentia-we-dev-keyvault"

# variables to use in key creation
$keyOps = "decrypt", "encrypt", "verify", "sign", "wrapKey", "unwrapKey"
$expires = (Get-Date).AddMonths(1).ToUniversalTime()
$Tags = @{'Severity' = 'High'; 'Application' = "DataLakeStore"}

# create new key in Vault
$key = Add-AzKeyVaultKey -Name "dlskey" -VaultName $keyvault.VaultName -Destination "Software" -KeyOps $keyOps -Expires $expires -Tag $Tags

$key.Id