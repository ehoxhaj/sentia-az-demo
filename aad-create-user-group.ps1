# Install-Module -Name AzureAD
Import-Module -Name AzureAD

Login-AzAccount
Get-AzSubscription -SubscriptionName "Azure for Students" | Select-AzSubscription

Connect-AzureAD

$addomain = "ermirhoxhajfshnstudent.onmicrosoft.com"
$PasswordProfile = New-Object -TypeName Microsoft.Open.AzureAD.Model.PasswordProfile
$PasswordProfile.Password = "P@ssword!23"
$PasswordProfile.ForceChangePasswordNextLogin = $true

$admin = New-AzureADUser -DisplayName "Administrator" -PasswordProfile $PasswordProfile -AccountEnabled $true -UserPrincipalName "administrator@$addomain" -MailNickName "AzureAdministrator"

$admingroup = New-AzureADGroup -DisplayName "Sentia Azure Cloud Admins" -MailEnabled $false -SecurityEnabled $true -MailNickName "AZCloudAdmins"

Add-AzureADGroupMember -ObjectId $admingroup.ObjectId -RefObjectId $admin.ObjectId

Add-AzureADGroupOwner -ObjectId $admingroup.ObjectId -RefObjectId $admin.ObjectId

Get-AzureADGroupMember -ObjectId $admingroup.ObjectId